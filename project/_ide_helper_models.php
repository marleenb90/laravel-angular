<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Bike
 *
 * @property int $id
 * @property string $make
 * @property string $model
 * @property string $year
 * @property string $mods
 * @property string $picture
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Builder $builder
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Item[] $items
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Rating[] $ratings
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike whereMake($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike whereMods($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bike whereYear($value)
 */
	class Bike extends \Eloquent {}
}

namespace App{
/**
 * App\Builder
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $location
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Bike $bike
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Builder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Builder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Builder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Builder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Builder whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Builder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Builder whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Builder whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Builder whereUpdatedAt($value)
 */
	class Builder extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Bike[] $bikes
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Item
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $company
 * @property int $bike_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Bike $bike
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereBikeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUpdatedAt($value)
 */
	class Item extends \Eloquent {}
}

namespace App{
/**
 * App\Rating
 *
 * @property int $id
 * @property int $user_id
 * @property int $bike_id
 * @property int $rating
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Bike $bike
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rating newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rating newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rating query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rating whereBikeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rating whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rating whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rating whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rating whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rating whereUserId($value)
 */
	class Rating extends \Eloquent {}
}

namespace App{
/**
 * App\Garage
 *
 * @property int $id
 * @property string $name
 * @property int $customer_level
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Bike[] $bikes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Garage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Garage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Garage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Garage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Garage whereCustomerLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Garage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Garage whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Garage whereUpdatedAt($value)
 */
	class Garage extends \Eloquent {}
}

